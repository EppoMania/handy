#!/bin/bash



# Packages
sudo apt-get install -y postgresql nginx supervisor git python3 python3-psycopg2 python3-pip python3-virtualenv virtualenvwrapper

# Database
sudo -u postgres createuser -DSR dsmrreader
sudo -u postgres createdb -O dsmrreader dsmrreader
sudo -u postgres psql -c "alter user dsmrreader with password 'dsmrreader';"

# System user
sudo useradd dsmr --home-dir /home/dsmr --create-home --shell /bin/bash
sudo usermod -a -G dialout dsmr

# Nginx
sudo mkdir -p /var/www/dsmrreader/static
sudo chown -R dsmr:dsmr /var/www/dsmrreader/

# Code checkout
sudo git clone https://github.com/dsmrreader/dsmr-reader.git /home/dsmr/dsmr-reader
sudo chown -R dsmr:dsmr /home/dsmr/

# Virtual env
sudo -u dsmr mkdir /home/dsmr/.virtualenvs
sudo -u dsmr virtualenv /home/dsmr/.virtualenvs/dsmrreader --system-site-packages --python python3
sudo sh -c 'echo "source ~/.virtualenvs/dsmrreader/bin/activate" >> /home/dsmr/.bashrc'
sudo sh -c 'echo "cd ~/dsmr-reader" >> /home/dsmr/.bashrc'

# Config
sudo -u dsmr cp /home/dsmr/dsmr-reader/dsmrreader/provisioning/django/settings.py.template /home/dsmr/dsmr-reader/dsmrreader/settings.py
sudo -u dsmr cp /home/dsmr/dsmr-reader/.env.template /home/dsmr/dsmr-reader/.env
sudo -u dsmr /home/dsmr/dsmr-reader/tools/generate-secret-key.sh

# Requirements
sudo -u dsmr /home/dsmr/.virtualenvs/dsmrreader/bin/pip3 install -r /home/dsmr/dsmr-reader/dsmrreader/provisioning/requirements/base.txt

# Setup
sudo -u dsmr /home/dsmr/.virtualenvs/dsmrreader/bin/python3 /home/dsmr/dsmr-reader/manage.py migrate
sudo -u dsmr /home/dsmr/.virtualenvs/dsmrreader/bin/python3 /home/dsmr/dsmr-reader/manage.py collectstatic --noinput

# Nginx
sudo rm /etc/nginx/sites-enabled/default
sudo cp /home/dsmr/dsmr-reader/dsmrreader/provisioning/nginx/dsmr-webinterface /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/dsmr-webinterface /etc/nginx/sites-enabled/
sudo nginx -t
sudo systemctl restart nginx.service

# Supervisor
sudo cp /home/dsmr/dsmr-reader/dsmrreader/provisioning/supervisor/dsmr_datalogger.conf /etc/supervisor/conf.d/
sudo cp /home/dsmr/dsmr-reader/dsmrreader/provisioning/supervisor/dsmr_backend.conf /etc/supervisor/conf.d/
sudo cp /home/dsmr/dsmr-reader/dsmrreader/provisioning/supervisor/dsmr_webinterface.conf /etc/supervisor/conf.d/
sudo supervisorctl reread
sudo supervisorctl update

# Create (super)user for the DSMR-reader configuration webinterface
sudo -u dsmr /home/dsmr/.virtualenvs/dsmrreader/bin/python3 /home/dsmr/dsmr-reader/manage.py createsuperuser --email dsmr@localhost --username admin

# You will be asked to choose and enter a password twice. The email address is not used.


