#!/bin/bash

##############
### START  ###
##############

echo "Start"

apt update -y
apt install htop -y

##############
### WEBMIN ###
##############

echo "INSTALL WEBMIN"

apt install gnupg -y

cp /etc/apt/sources.list /etc/apt/sources.list.bak
echo "" >> /etc/apt/sources.list
echo "# Webmin added by Script van Jeroen van Noort" >> /etc/apt/sources.list
echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list

wget http://www.webmin.com/jcameron-key.asc
apt-key add jcameron-key.asc
apt-get update -y
apt-get install webmin -y

rm jcameron-key.asc

echo "Installation Webmin is done. Please go to: https://(ipadres/jouw.domein.nl):10000 en check in."

exit 0

